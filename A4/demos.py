#!/usr/bin/env python3
from functions import get_reqs, data_analysis_2

def main():
    get_reqs()
    data_analysis_2()

main()