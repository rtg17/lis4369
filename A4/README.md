# LIS4369 - A4

## A4 Requirements:

*[Further Data Analysis](/A4):*

- Import a remote .csv file

- Deep dive into data selection and parsing with data frames

- Take advantage of n-dimensional arrays

#### Assignment Screenshots:

![Running in Bash (assumed Python interpreter is in PATH)](/img/a4_idle.png)

![Running in IDLE (Editor/interpreter bundled with Python)](/img/a4_bash.png)