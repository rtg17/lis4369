from re import search
import numpy
from pandas import read_csv
from matplotlib.pyplot import legend, show
from matplotlib import style

def get_reqs():
    print('Data Analysis 2\n\n' +
        'Program/Assignment Requirements\n' +
        '1. Create and run a demo.py file\n' +
        '2. Ensure all neccesary modules are installed\n' +
        '3. Test the package installer (pip freeze)\n' +
        '4. Create three functions:\n'
        '\ta. main(): The primary function (demos.py)\n' +
        '\tb. get_reqs(): Displays this text (functions.py)\n' +
        '\tc. data_analysis_2(): Primary function of the program (functions.py)\n' +
        '6. Display graph per instructions within demo.py\n')

def data_analysis_2():
    df = read_csv('https://raw.githubusercontent.com/vincentarelbundock/Rdatasets/master/csv/Stat2Data/Titanic.csv')
    print('\n1. Print indexes\n', df.index)
    print('\n2. Print columns\n', df.columns)
    print('\n3. Print columns (using slicing notation)\n', df.columns[:])
    print('\n4. Print values\n', df.values)
    print('\n5. Print component data types')
    print('\ta. Index type\n\t', type(df.index))
    print('\tb. Column type\n\t', type(df.columns))
    print('\ta. Value type\n\t', type(df.values))
    print('\n6. Print summary of data frame\n', df.info())
    print('\n7. Print first five lines of all columns\n', df.head())
    df = df.drop('Unnamed: 0', 1)
    print('\n8. Drop a column in the data frame, then reprint summary\n', df.info())
    print('\n9. Print first five lines after dropping column\n', df.head())
    print('\n10. Using iloc (precise data selection), print first three rows\n', df.iloc[:3])
    print('\n11. Using iloc, print last three rows\n', df.iloc[1310:])
    print('\n12. Using iloc, select rows 1, 3, 5 and columns 2, 4 ,6\n', df.iloc[[0, 2, 4], [1, 3, 5]]) # Arrays are zero indexed
    print('\n13. Using iloc, select all rows and coulums 2, 4, and 6\n', df.iloc[:, [1, 3, 5]])
    print('\n14. Using iloc, selects rows 1, 3, and 5 and all columns\n', df.iloc[[0, 2, 4], :])
    print('\n15. Using iloc, select all rows and columns\n', df.iloc[:, :])
    print('\n16. Using iloc, select all rows and all columns after 2\n', df.iloc[:, 1:])
    print('\n17. Using iloc, select only the first row and column\n', df.iloc[0:1, 0:1])
    print('\n18. Using iloc, select only rows and columns 3 to 5 and store them into a variable (a)')
    a = df.iloc[2:5, 2:5]
    print('\n19. Using iloc, convert all rows and all columns after 2 into a NumPy n-dimensional array and store them into a variable (b)')
    b = df.iloc[:, 1:].values
    print(b)
    print('\n20. Print type of data frame\n', type(df))
    print('\n21. Print type of df with selected rows and columns\n', type(a))
    print('\n22. Print type of n-dimensional array\n', type(b))
    print('\n23. Print dimensions of n-dimensiona array\n', b.shape)
    print('\n24. Print types of items in n-dimensional array\n', b.dtype)
    print('\n25. Print a (stores selected rows/columns of data frame)\n', a)
    print('\n26. Print length of a\n', len(a))
    print('\n27. Print b (stores n-dimensional array)\n', b)
    print('\n28. Print length of b\n', len(b))
    print('\n29. Print elements 1, 2 of n-dimensional array\n', b[1, 2])
    print('\n30. Print all records of n-dimensional array at column 2\n', b[:, 1])
    print('\n31. Get passenger names from dataframe using associative methods')
    names = df['Name']
    print(names)
    print('\n32. Find all passengers with the name "Allison" using regular expressions')
    for name in names:
        print(search(r'(Allison)', name))
    print('\n33. Print average age of passengers')
    avg = df['Age'].mean()
    print(avg)
    print('\n34. Print average age, rounded two decimal places\n', avg.round(2))
    print('\n35. Print mean of every column in data frame\n', df.mean(axis=0))
    print('\n36. Print summary of age statistic\n', df['Age'].describe())
    print('\n37. Print minimum age statistic\n', df['Age'].min())
    print('\n38. Print maximum age statistic\n', df['Age'].max())
    print('\n39. Print median age statistic\n', df['Age'].median())
    print('\n40. Print mode of age statistic\n', df['Age'].mode())
    print('\n41. Print number of values in age statistic\n', df['Age'].count)
    print('\n42. Display ages of first 20 passenger through graph')

    style.use('ggplot')
    df[:21].plot()
    legend()
    show()
