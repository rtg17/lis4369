#!/usr/bin/env python3

def get_requirements():
    print('Payroll Calculator\n\nProgram Requirements:\n1. Must use float data type for user input\n2. Overtime rate: 1.5 times hourly rate (hours over 40)\n3. Holiday rate: 2.0 times hourly rate (all holiday hours)\n4. Must format currency with dollar sign, and round to two decimal places\n5. Create at least 3 functions that are called by the program:\n\t1. main(): calls at least two other functions\n\tb. get_requirements(): Displays program requirements (this text)\n\tc. calculate_payroll(): Calculates an individual\'s one-week paycheck\n')

def calculate_payroll():
    overtimeHours = 0.0

    print('\nInput:')
    normalHours = float(input('Enter non-holiday hours worked: '))
    holidayHours = float(input('Enter holiday hours worked: '))
    payRate = float(input('Enter hourly pay rate: '))
    if normalHours > 40:
        overtimeHours = normalHours - 40
        normalHours = normalHours - overtimeHours

    normalPayroll = round(payRate * normalHours, 2)
    overtimePayroll = round((payRate * 1.5) * overtimeHours, 2)
    holidayPayroll = round((payRate * 2.0) * holidayHours, 2)

    print('\nOutput:')
    print('{0:12} ${1:,.2f}'.format('Base:', normalPayroll))
    print('{0:12} ${1:,.2f}'.format('Overtime:', overtimePayroll))
    print('{0:12} ${1:,.2f}'.format('Holiday:', holidayPayroll))
    print('{0:12} ${1:,.2f}'.format('Gross:', normalPayroll + holidayPayroll + overtimePayroll))
    