# LIS4369 - A2

## A2 Requirements:

*[Utilize imports with Python](/A2):*

- Utilize imports with Python

#### Assignment Screenshots:

![AMPPS Installation Screenshot](/img/a2_idle.png)

![JDK Installation Screenshot](/img/a2_bash.png)