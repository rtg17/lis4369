#!/usr/bin/env python3
## Author:  Rhett Gordon
## Date:    9/9/2019
## Project: Assignment 1 for LIS4369 (Introduction to Python)

# Neccesary output
print('\n\nTip Calculator\n\n')
print('Program Requirements:')
print('1. Must use float data type for user input (except, "Party Number")')
print('2. Must round calculation to two decimal places')
print('3. Must format currency with dollar sign, and two decimal places')

# User input time
print('\nUser Input:')
cost = float(input('Cost of meal: '))
tax = float(input('Tax percent: '))
tip = float(input('Tip percent: '))
party = int(input('Party number: '))

# Calculations
tax = round(cost * (tax / 100), 2)
due = round(cost + tax, 2)
tip = round(due * (tip / 100), 2)

# Program output time
print('\nProgram output:')
print('Subtotal: $' + "%0.2f" % cost)
print('Tax: $' + "%0.2f" % tax)
print('Amount due: $' + "%0.2f" % due)
print('Gratuity: $' + "%0.2f" % tip)
print('Total: $' + "%0.2f" % (due + tip))
print('Split (' + str(party) + '): $' + "%0.2f" % (round((due + tip) / party, 2)))