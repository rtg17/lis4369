# LIS4369 - A1

## A1 Requirements:

*[Set up development environment and test Python out](/A1):*

- Install Python environment, R, and R Studio
- Create a basic I/O Python script
- Show the script running in IDLE and other terminal environments

### Git commands w/short descriptions:

1. `git init` - Initilalize the repo locally
2. `git status` - Allows you to see the current state of your repo
3. `git add` - Stage files for a commit
    - `git add .` to stage any file not regarded to in a [`.gitignore`](/.gitignore) file
    - `git add` with any other names of files, in any number, to stage them manually
4. `git commit` - Sets staged file changes (adds, deletions, and moficiations) for a new point in history
5. `git push` - Pushes commit history to a remote repo if one is set
    - `git push origin branch-in-question`
6. `git pull` - Pulls commit history from a remote repo if one is set
7. `git branch` - Create a new branch from your current one in order to commit changes within it's own box
8. `git merge` - Merges a branch's commit history with it's parent
    - `git merge child parent` from the `parent` branch

#### Assignment Screenshots:

![AMPPS Installation Screenshot](/img/a1_idle.png)

![JDK Installation Screenshot](/img/a1_bash.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rtg17/bitbucketstationlocations/ "Bitbucket Station Locations")