SQFT_PER_GALLON = 350
FORMATTED_TWO = '{0:20} {1:>10}'
FORMATTED_THREE = '{0:10} {1:>8} {2:>12}'
FORMATTED_THREE_ROWS = '{0:10}$ {1:>8.2f} {2:>12.2%}'
int_sqft = 0.0
ppg_paint = 0.0
hourly_rate = 0.0

def get_requirements():
    print('Painting Estimator\n\nProgram Requirements:\n1. Calculate home interior paint color\n2. Must use float data type\n3. Must use SQFT_PER_GALLON constant (350)\n4. Must use iteration structure (aka loop)\n5. Format, right-align numbers, and round to two decimal places\n6. Create at least five functions that are called by the program.\n\ta. main(): call two other functions: get_requirements() and estimate_painting_cost()\n\tb. get_requirements(): displays this\n\tc. estimate_painting_cost(): calculate interior home painting and calls print functions.\n\td. print_painting_estimate(): display painting costs.\n\te. print_painting_percentage(): display paint costs percentages\n')

def estimate_painting_cost():
    flag = 'y'

    while (flag == 'y'):
        print('Input')
        global int_sqft, ppg_paint, hourly_rate
        int_sqft = float(input('Enter total interior sq ft: '))
        ppg_paint = float(input('Enter price per gallon: '))
        hourly_rate = float(input('Enter hourly painting rate per sq ft: '))

        print_painting_estimate()
        print_painting_percentage()

        flag = check_continuity()
    # end of while loop

def print_painting_estimate():
    print('\nOutput:')
    print(FORMATTED_TWO.format('Item', 'Amount'))
    print(FORMATTED_TWO.format('Total Sq Ft:', "%0.2f" % int_sqft))
    print(FORMATTED_TWO.format('Sq Ft Per Gallon:', "%0.2f" % SQFT_PER_GALLON))
    print(FORMATTED_TWO.format('Number of Gallons:', "%0.2f" % round(int_sqft / SQFT_PER_GALLON, 2)))
    print(FORMATTED_TWO.format('Paint per Gallon:', "%0.2f" % ppg_paint))
    print(FORMATTED_TWO.format('Paint per Gallon:', "%0.2f" % hourly_rate))

def print_painting_percentage():
    paint = (int_sqft / SQFT_PER_GALLON) * ppg_paint
    labor = int_sqft * hourly_rate
    total = paint + labor
    print('\n')
    print(FORMATTED_THREE.format('Cost', 'Amount', 'Percentage'))
    print(FORMATTED_THREE_ROWS.format('Paint:', round(paint, 2), paint / total))
    print(FORMATTED_THREE_ROWS.format('Labor:', round(labor, 2), labor / total))
    print(FORMATTED_THREE_ROWS.format('Total:', round(total, 2), (paint / total) + (labor / total)))

def check_continuity():
    while ('true' == 'true'):
        inp = input('\nEstimate another paint job? (y/n): ')
        if (inp == 'y'):
            print()
            return 'y'
        elif (inp == 'n'):
            print('\nThank you for using our Painting Estimator!')
            return 'n'
        else:
            print('Not a valid input!')