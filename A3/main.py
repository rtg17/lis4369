#!/usr/bin/env python3
import functions as f

def main():
    f.get_requirements()
    f.estimate_painting_cost()

if __name__ == "__main__":
    main()