# LIS4369 - A3

## A3 Requirements:

*[Utilize imports with Python](/A3):*

- Utilize lots of string formatting types

- Utilize globals and handle global scoping

- Implement loops that run until a logcal end-state is reached (i.e. user decides to stop using the program)

#### Assignment Screenshots:

![Running in Bash (assumed Python interpreter is in PATH)](/img/a3_idle.png)

![Running in IDLE (Editor/interpreter bundled with Python)](/img/a3_bash.png)