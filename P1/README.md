# LIS4369 - P1

## P1 Requirements:

*[Utilize imports with Python](/P1):*

- Utilize imports from remote modules

- Plot data pulled from a remote resource (using pandas and matplotlib)

#### Assignment Screenshots:

![Running in Bash (assumed Python interpreter is in PATH)](/img/p1_idle.png)

![Running in IDLE (Editor/interpreter bundled with Python)](/img/p1_bash.png)

![The data, plotted via matplotlib](/img/p1_graph.png)