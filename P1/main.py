import pandas as pd
from datetime import datetime
import pandas_datareader as pdr
import matplotlib.pyplot as plt
from matplotlib import style

def get_requirements():
    print('Program Requirements:\n'
        + '1. Run demo.py\n'
        + '2. If errors, more than likely missing installations.\n'
        + '3. Test Python Package Installer: pip freeze.\n'
        + '4. Research how to do the following installations:\n'
        + '\ta. pandas (only if missing)\n'
        + '\tb. pandas-datareader (only if missing)\n'
        + '\tc. matplotlib (only if missing)\n'
        + '5. Create at lease three functions that are called by the program:\n'
        + '\ta. main(): calls at least two other functions.\n'
        + '\tb. get_requirements(): display the program requirements (this text).\n'
        + '\tc. data_analysis_1(): Displays the following data')

def plot_data(data):
    style.use('ggplot')
    data['High'].plot()
    data['Adj Close'].plot()
    plt.legend()
    plt.show()

def data_analysis_1():
    start = datetime(2010, 1, 1)
    end = datetime.now()

    df = pdr.DataReader('XOM', 'yahoo', start, end)

    print('\nPrint number of records:\n', df.columns)
    print('\nPrint data frame:\n', df)
    print('\nPrint first five lines:\n', df.head(5))
    print('\nPrint last two lines:\n', df.tail(2))

    plot_data(df)

def main(): 
    get_requirements()
    data_analysis_1()

main()