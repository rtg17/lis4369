# LIS4369 - Extensible Enterprise Solutions

## Rhett Gordon

### Course Work

1) [A1](/A1) ([README.md](/A1/README.md))

    - Install environment software with evidence

    - Show basic literacy with Python

    - Provite git command descriptions

2) [A2](/A2) ([README.md](/A2/README.md))

    - Utilize imports with Python

3) [A3](/A3) ([README.md](/A3/README.md))

    - Utilize lots of string formatting types

    - Utilize globals and handle global scoping

    - Implement loops that run until a logcal end-state is reached (i.e. user decides to stop using the program)

4) [A4](/A4) ([README.md](/A4/README.md))

    - Import a remote .csv file

    - Deep dive into data selection and parsing with data frames

    - Take advantage of n-dimensional arrays

5) [A5](/A5) ([README.md](/A5/README.md))

    - Get started using R and R Studio

    - Create a file with proper syntax and output for R

6) [P1](/P1) ([README.md](/P1/README.md))

    - Import and utilize external modules from a package registrar
    
    - Plot data pulled from a remote resource (using pandas and matplotlib)

7) [P2](/P2) ([README.md](/P2/README.md))

    - Research and implement data science methods in R

    - Create and style/label graphs with data, using R's inherent `plot()` function as well as ggplot2's `qplot()` and `labs()`

8) [Skillsets](/skillsets)