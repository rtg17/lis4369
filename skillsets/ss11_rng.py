#!/bin/bash/env python3
from random import randint, shuffle
start, end = ( 0, 0 )

def displayRandInt(s, e):
    result = []

    for i in range(s, e + 1):
        print(randint(s, e), sep=", ", end=" ")

def displayShuffle(s, e):
    result = []

    for i in range(s, e + 1):
        result.append(i)
    
    shuffle(result)

    for i in result:
        print(i, sep=", ", end=" ")

print('Psuedo-Random Number Generator\n\n' +
    '1. Get user input for start and end integers\n' +
    '2. Must display 10 pseudo-random numbers between both values\n' +
    '3. Must use integer data types\n' +
    '4. Example 1 must use range() and randint()\n' +
    '5. Example 2 must use range() and shuffle()\n')

print('Input:')
while True:
    first_hold = input('Enter start value: ')
    if first_hold.isdigit() == False:
        print('Invalid input!')
    else:
        start = int(first_hold)
        break

while True:
    second_hold = input('Enter end value: ')
    if second_hold.isdigit() == False:
        print('Invalid input!')
    elif int(second_hold) == start:
        print('Pick a bigger number!')
    else:
        end = int(second_hold)
        break

print('\nOutput:')
print('Example 1: ', end="")
displayRandInt(start, end)
print('\nExample 2: ', end="")
displayShuffle(start, end)