#!/use/bin/env python3 

print('Python Tuples\n\n'
    + '1. Tuples (Python data structures): *immutable* (cannot be changed!), ordered sequence of elements\n'
    + '2. Tuples are immutable/unchangeable -- that is, cannot insert, update, or delete elements\n'
    + '3. Create tuple using parenthesis (tuple) -- my_tuple1 = ( \'cherries\', \'apples\', \'bananas\', \'oranges\' )'
    + '4. Create tuple (packing), that is, *without* using parenthesis (aka tuple "packing"): my_tuple2 = 1, 2, "three", "four"\n'
    + '5. Pythin tuple (unpacking), that is, assign value from tuple to sequence of variables: myfruit1, myfruit2, myfruit3, myfruit4 = my_tuple1\n'
    + '6. Create a program that mirrors the following IPO format')

my_tuple1 = ( 'cherries', 'apples', 'bananas', 'oranges' )
my_tuple2 = ( 1, 2, 'three', 'four' )

print('\nPrint my_tuple1\n', my_tuple1)
print('\nPrint my_tuple2\n', my_tuple2)

myfruit1, myfruit2, myfruit3, myfruit4 = my_tuple1
print('\nPrint my_tuple1 unpacking\n', myfruit1, myfruit2, myfruit3, myfruit4)

print('\nPrint third element in my_tuple2\n', my_tuple2[2])

print('\nPrint "slice" of my_tuple1\n', my_tuple1[1:3])

print('\nReassign my_tuple2 using parenthesis.')
my_tuple2 = ( 1, 2, 3, 4 )
print('Print my_tuple2:\n', my_tuple2)

print('\nReassign my_tuple2 using packing method (no parenthesis)')
my_tuple2 = 5, 6, 7, 8
print('Print my_tuple2:\n', my_tuple2)

print('\nPrint number of elements in my_tuple1: ', len(my_tuple1))

print('\nPrint type of my_tuple1: ', type(my_tuple1))
