#!/usr/bin/env python3
from math import pi, pow

def get_reqs():
    print('Sphere Volume Calculator\n\n'
    + '1. Calculate sphere volume into liquid US gallons from user-entered diameter value in inches\n'
    + '2. Must use Python\'s built-in pi and pow() function\n'
    + '3. Must check for non-integer input from user\n'
    + '4. Must continually prompt the user for input until the user declines use of the program\n')

def calc_volume():
    action_completed = False
    user_choice = None

    while action_completed == False:
        user_choice = None
        num = input('Please enter a diameter in inches: ')
        if num.isnumeric():
            print('\nSpheric volume:', round((4/3) * pi * pow(int(num) / 2, 3) * 0.004329, 2), 'liquid US gallons')
            while user_choice == None:
                user_choice = input('\nDo you want to enter another sphere volume? (y/n) ').lower()
                if user_choice.isalpha() and user_choice == 'y':
                    print() # Nothing
                elif user_choice.isalpha() and user_choice == 'n':
                    return False
                else:
                    print('\nNot a valid input!')
                    user_choice = None
        else:
            print('\nNot a valid integer!\n')

def main():
    currently_using = True
    
    get_reqs()

    while currently_using:
        user_choice = input('Do you want to calculate a sphere\'s volume? (y/n) ').lower()
        if user_choice.isalpha() and user_choice == 'y':
            print()
            currently_using = calc_volume()
        elif user_choice.isalpha() and user_choice == 'n':
            currently_using = False
        else:
            print('\nNot a valid input!\n')
    
    # Logical end of program
    print('\nThank you for using the Sphere Volume Calculator')

if __name__ == "__main__":
    main()