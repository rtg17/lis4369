#!/use/bin/env python3 
print('Python Dictionaries\n\n1. Kinda like JavaScript objects, in that object properties encapsulated as a string are called via associate array methods\n2. Values in a dictionary must be mapped to a given string representing it\n3. Keys are immutable and can be represented by strings, numbers, or tuples\n4. The values of those keys are not immutable and can be changed at any time.\nGoal: create an empty dictionary, then assign values based on the needed keys (fname, lname, degree, major, and gpa)\n')
my_dictionary = {}

print('Input:')
my_dictionary['fname'] = input('First name: ')
my_dictionary['lname'] = input('Last name: ')
my_dictionary['degree'] = input('Degree: ')
my_dictionary['major'] = input('Major: ')
my_dictionary['gpa'] = float(input('GPA: '))

print('Output:')
print('Print my_dictionary:\n', my_dictionary)
print('\nReturn view of dictionary\'s (key, value) pair, built-in function:\n', my_dictionary.items())
print('\nReturn view of dictionary\'s keys, built-in function:\n', my_dictionary.keys())
print('\nReturn view of dictionary\'s values pair, built-in function:\n', my_dictionary.values())
print('\nPrint only first and last name using keys:\n', my_dictionary['fname'] + ' ' + my_dictionary['lname'])
print('\nPrint only first and last name, using get function:\n', my_dictionary.get('fname') + ' ' + my_dictionary.get('lname'))
print('\nCount the number of items in my_dictionary:\n', len(my_dictionary))
my_dictionary.popitem()
print('\nRemove the last item (popitem):\n', my_dictionary)
my_dictionary.pop('major')
print('\nDelete major from dictionary, using key:\n', my_dictionary)
print('\nReturn object type:\n', type(my_dictionary))