#!/usr/bin/env python3
from math import pow
op = ''
flag = False

print('Python Selection Structures with checks\n\nProgram Requirements:\n1. Use Python selection structures\n2. Prompt user for two numbers and a suitable operator\n3. Test for correct numeric operator\n4. Replicate display given in image\n5. Utilize data validation\n\nPython Calculator')

while flag == False :
    try:
        numOne = float(input('Enter num1: '))
        flag = True
    except ValueError:
        print('This is not a valid int or float!')

while flag == True:
    try:
        numTwo = float(input('Enter num2: '))
        flag = False
    except ValueError:
        print('This is not a valid int or float!')

print('\nSuitable operators: +, -, *, /, // (int division), % (modulo), ** (power)')

while flag == False :
    try:
        op = input('Enter operator: ')
        if (op == '+') :
            print(numOne + numTwo)
            flag = True
        elif (op == '-') :
            print(numOne - numTwo)
            flag = True
        elif (op == '*') :
            print(numOne * numTwo)
            flag = True
        elif (op == '/') :
            print(numOne / numTwo)
            flag = True
        elif (op == '//') :
            print(numOne // numTwo)
            flag = True
        elif (op == '%') :
            print(numOne & numTwo)
            flag = True
        elif (op == '**') :
            print(numOne ** numTwo)
            print(pow(numOne, numTwo))
            flag = True
        else : 
            print('Invalid operator')
    except ZeroDivisionError:
        print('Cannot divide by zero!')
        while flag == False:
            try:
                numTwo = float(input('Enter num2: '))
                # Flagging is in the conditional only because, if solar flares mess something up, we should probably re-prompt the user for an operator
                if op == '/':
                    print(numOne / numTwo)
                    flag = True
                elif op == '//':
                    print(numOne // numTwo)
                    flag = True
            except ValueError:
                print('This is not a valid int or float!')
    except ArithmeticError:
        print('Error in arithmetic calculation!')

print('Thank you for using the Math Calculator')