# LIS4369 - Skillsets

- Skillset #1: [Square Feet to Acres](./ss1_square_feet_to_acres.py)
- Skillset #2: [Miles Per Gallon](./ss2_mpg.py)
- Skillset #3: [IT/ICT Split](./ss3_it_ict_split.py)
- Skillset #4: [Calorie Calculator](./ss4_calories.py)
- Skillset #5: [Selection Structures](./ss5_selections.py)