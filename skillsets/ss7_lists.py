#!/usr/bin/env python3

# Define the list we will rely on
my_list = [ ]

print('Python Lists\n\nProgram Requirements:')
print('1. Lists (Python data structure): mutable, ordered sequence of elements')
print('2. Lists are mutable/changeable -- that is, can insert, update, and delete')
print('3. Create list - using square brackets [list]: my_list = [ "cherries", "apples", "bananas", "oranges" ]')
print('4. Create a program that mirrors the following IPO (input/process/output) format.')
print('Note: user enters number of required list elements, dynamically rendered below (that is, number of elements can change each run)\n')

# Dynamic input using for-loop
print('Input:')
list_length = int(input('Enter number of list elements: '))

print()
for i in range(1, list_length + 1):
    my_list.append(input('Please enter list element ' + str(i) + ': '))

print('\nOutput:\nPrint my_list\n', my_list, '\n')

# Implement a new term within the range
new_term = input('Please enter list element: ')
term_index = int(input('Please enter list *index* position: '))

my_list.insert(term_index, new_term)

print('\nInsert element into specific position in my_list\n', my_list, '\n')

# Print number of list elements
print('Count number of elements in list\n', len(my_list), '\n')

# Sort elements alphabetically
my_list.sort()
print('Sort elements in list alphabetically\n', my_list, '\n')

# Reverse list
my_list.reverse()
print('Reverse list\n', my_list, '\n')

# Remove last list element
my_list.pop()
print('Remove last list element\n', my_list, '\n')

# Delete second element
my_list.__delitem__(1)
print('Delete second element from list by index\n', my_list, '\n')

# Delete element from list by value
my_list.remove('cherries')
print('Delete element from list by value (cherries)\n', my_list, '\n')

# deleto
my_list.clear()
print('Delete all elements from list:\n', my_list, '\n')