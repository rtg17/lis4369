#!/usr/bin/env python3

# Skillset 2 Functions
def get_reqs():
    print('Miles Per Gallon\n\nProgram Requirements:\n1. Convert MPG.\n2. Must use float datatype for user input and calculation.\n3. Format and round conversion to two decimal places\n\n')

def calc():
    print('Input:')
    miles=float(input('Enter miles driven: '))
    gallons=float(input('Enter gallons of fuel used: '))
    print('\nOutput:\n'+'{:,.2f}'.format(miles)+' miles driven and '+'{:,.2f}'.format(gallons)+' gallons used = '+'{:,.2f}'.format(round(miles/gallons,2))+' mpg\n\n')