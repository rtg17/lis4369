#!/use/bin/env python3 

print('Pythin Sets - like mathematical sets\n\n'
        + '1. Sets (Python data structure): mutable, heterogeneous, unordered sequence of elements, *but* cannot hold duplicate values.\n'
        + '2. Sets can insert, update, and delete elements.\n'
        + '3. While sets are mutable, they cannot contain other mutable items such as lists, dictionaries, and other sets. Elements *must* be immutable.\n'
        + '4. Sets are unordered, and thus cannot use indexing or slicing to modify it\'s elements\n'
        + '5. Two methods can be used to make sets:\n'
        + '\ta. Create the set using curly braces (\{ 1, 3.14, 2, \'four\', \'Five\' \})\n'
        + '\tb. Create the set using the set() method, passing either a list or tuple (set([ 1, 3.14, 2, \'four\', \'Five\' ]))\n'
        + '6. Create a program which reverse-engineers the image which was provided.\n'
        + 'NOTE: An iterable is *any* object which can be iterated over, which includes lists, tuples, and sets.\n')

my_set = { 1, 2.0, 3.14, 'Five', 'four' }
print('\nPrint my_set created using curly brackets:\n', my_set)

print('\nPrint type of my_set:\n', type(my_set))

setted_list = [ 1, 2.0, 3.14, 'Five', 'four' ]
my_set1 = set(setted_list)
print('\nPrint my_set created using set() function with list:\n', my_set1)

print('\nPrint type of my_set1:\n', type(my_set1))

setted_tuple = ( 1, 2.0, 3.14, 'Five', 'four' )
my_set2 = set(setted_tuple)
print('\nPrint my_set2 created using set() function with tuple:\n', my_set2)

print('\nPrint type of my_set2:\n', type(my_set2))

print('\nLength of my_set:\n', len(my_set))

my_set.discard('four')
print('\nDiscard \'four\':\n', my_set)

my_set.remove('Five')
print('\nRemove \'Five\':\n', my_set)

print('\nLength of my_set:\n', len(my_set))

my_set.add(4)
print('\nAdd element to set (4) using add() method:\n', my_set)

print('\nLength of my_set:\n', len(my_set))

print('\nDisplay minimum number:\n', min(my_set))

print('\nDisplay maximum number:\n', max(my_set))

print('\nDisplay sum of numbers:\n', sum(my_set))

my_set.clear()
print('\nDelete all set elements:\n', my_set)

print('\nLength of my_set:\n', len(my_set))