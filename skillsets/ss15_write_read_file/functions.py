from os import path

address = 'President Abraham Lincoln\'s Gettysburg Address:\nFour score and seven years ago our fathers brought forth on this continent, a new nation, conceived in Liberty, and dedicated to the proposition that all men are created equal.\n\nNow we are engaged in a great civil war, testing whether that nation, or any nation so conceived and dedicated, can long endure. We are met on a great battle-field of that war. We have come to dedicate a portion of that field, as a final resting place for those who here gave their lives that that nation might live. It is altogether fitting and proper that we should do this.\n\nBut, in a larger sense, we can not dedicate -- we can not consecrate -- we can not hallow -- this ground. The brave men, living and dead, who struggled here, have consecrated it, far above our poor power to add or detract. The world will little note, nor long remember what we say here, but it can never forget what they did here. It is for us the living, rather, to be dedicated here to the unfinished work which they who fought here have thus far so nobly advanced. It is rather for us to be here dedicated to the great task remaining before us -- that from these honored dead we take increased devotion to that cause for which they gave the last full measure of devotion -- that we here highly resolve that these dead shall not have died in vain -- that this nation, under God, shall have a new birth of freedom -- and that government of the people, by the people, for the people, shall not perish from the earth.\n\nAbraham Lincoln\nNovember 19, 1863'
filename = 'skillsets/ss15_write_read_file/gettysburg_address.txt'

def write_read_file():
    """
    write_read_file() (Required function)
        Params: none
        Function type: void
        Usage: Runs two functions
            1. write_address()
            2. read_address()     
    """
    write_address()
    read_address()

def write_address():
    """
    write_address() (Required function)
        Params: none
        Function type: void
        Usage: Creates a new file named "gettysburg_address.txt" and write's Abraham Lincoln's Gettysburg Address to it, encoded in UTF-8
    """
    try:
        write_stream = open(filename, 'w', encoding='utf-8')
        write_stream.write(address)
        write_stream.close()
    except OSError:
        print('Could not write a file. Is this in a protected directory?')

def read_address():
    """
    read_address() (Required function)
        Params: none
        Function type: void
        Usage: Opens a file named "gettysburg_address.txt" and prints the output to the terminal. Also prints the absolute file location.
    """
    try:
        read_stream = open(filename, 'r')
        print(read_stream.read())
        read_stream.close()

        print('\nFull file path:\n', path.dirname(path.abspath(__file__)) + '/' + filename) 
    except OSError:
        print('Could not read the file. Was it intermittently moved or deleted?')

def get_reqs():
    """Display the program requirements for this skillset"""
    print('Program Requirements:\n'
        + '1. Create a subdirectory with a main.py and functions.py file\n'
        + '2. Utilize Abraham Lincoln\'s Gettysburg Address as a peice of data for this skillset\n'
        + '3. Write the address to a file, then read the address from the same file output\n'
        + '4. Create and display Python docstrings for the functions utilized in the program\n'
        + '5. Display the full file path of the address\n'
        + '6. Replicate the display given in the skillset image\n')
    
    print(write_read_file.__doc__)
    print(write_address.__doc__)
    print(read_address.__doc__)