#!/usr/bin/env python3
from functions import get_reqs, write_read_file

def main(): 
    get_reqs()
    write_read_file()

if __name__ == '__main__':
    main()
