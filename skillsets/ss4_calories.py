#!/usr/bin/env python3
from ss4_functions import calculate_calories

def main():
    print('Calorie Percentage:\n\nProgram Requirements:\n1. Find calories per grams of fat, carbs, and protein\n2. Calculate percentages\n3. Must use float data types\n4. Format, right-align numbers, and round to two decimal places\n')
    calculate_calories()


if __name__ == "__main__":
    main()