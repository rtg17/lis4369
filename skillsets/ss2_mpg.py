#!/usr/bin/env python3
import ss2_functions as mpg

def main():
    mpg.get_reqs()
    mpg.calc()

if __name__ == "__main__":
    main()