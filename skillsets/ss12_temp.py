#!/usr/bin/env python3

def show_reqs():
    print('Temperature conversion program\n\n'
    + '1. Program converts user-entered temp from either F to C, or C to F\n'
    + '2. Program continues to prompt for input until a user stops the loop with \'n\'\n'
    + '3. Note: Allow upper and lower-case input, just don\'t permit incorrect entries\n'
    + '4. Note: Program does not validate numeraic data\n\n')

def temp_conv():
    temp = 0.0

    print('Input')
    choice = input('Do you want to convert a temperature? (y/n) ').lower()

    while choice[0] == 'y':
        typ = input('for Fahrenheit to Celsius, type f. For Celsius to Fahrenheit, type c: ').lower()

        if typ[0] == 'f':
            temp = float(input('Enter temperature in celsius: '))
            print('Temperature in fahrenheit: ', ((temp - 32) * 5)/9)
            choice = input('Do you want to convert another temperature? (y/n) ').lower()
        
        elif typ[0] == 'c':
            temp = float(input('Enter temperature in fahrenheit: '))
            print('Temperature in celsius: ', (temp * 9/5) + 32)
            choice = input('Do you want to convert another temperature? (y/n) ').lower()

        else:
            print('Incorrect entry. Please try again')
            choice = input('Do you want to convert a temperature? (y/n) ').lower()

    print('\nThank you for using the tempurature conversion program!')

    
def main():
    show_reqs()
    temp_conv()


main()