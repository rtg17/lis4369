#!/usr/bin/env python3
from math import pow
op = ''
flag = 'false'

print('Python Selection Structures\n\nProgram Requirements:\n1. Use Python selection structures\n2. Prompt user for two numbers and a suitable operator\n3. Test for correct numeric operator\n4. Replicate display given in image\n\nPython Calculator')

numOne = float(input('Enter num1: '))
numTwo = float(input('Enter num2: '))

print('\nSuitable operators: +, -, *, /, // (int division), % (modulo), ** (power)')

while flag == 'false' :
    op = input('Enter operator: ')
    if (op == '+') :
        print(numOne + numTwo)
        flag = 'true'
    elif (op == '-') :
        print(numOne - numTwo)
        flag = 'true'
    elif (op == '*') :
        print(numOne * numTwo)
        flag = 'true'
    elif (op == '/') :
        print(numOne / numTwo)
        flag = 'true'
    elif (op == '//') :
        print(numOne // numTwo)
        flag = 'true'
    elif (op == '%') :
        print(numOne & numTwo)
        flag = 'true'
    elif (op == '**') :
        print(numOne ** numTwo)
        print(pow(numOne, numTwo))
        flag = 'true'
    else : 
        print('Invalid operator')