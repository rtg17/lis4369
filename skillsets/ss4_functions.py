def calculate_calories():
    print('Input:')
    fatCount = float(input('Enter total fat grams: '))
    carbCount = float(input('Enter total carb grams: '))
    proteinCount = float(input('Enter total protein grams: '))

    fatTotal = fatCount * 9
    carbTotal = carbCount * 4
    proteinTotal = proteinCount * 4
    total = fatTotal + carbTotal + proteinTotal

    print('\nOutput:')
    print('{0:8} {1:>10} {2:>13}'.format('Type', 'Calories', 'Percentage'))
    print('{0:8} {1:10,.2f} {2:13.2%}'.format('Fat', fatTotal, fatTotal / total))
    print('{0:8} {1:10,.2f} {2:13.2%}'.format('Carbs', carbTotal, carbTotal / total))
    print('{0:8} {1:10,.2f} {2:13.2%}'.format('Protein', proteinTotal, proteinTotal / total))