#!/usr/bin/env python3

# Function definitions
def implicit_list():
    lis = []
    for i in range(0, 3):
        lis.append(i + 1)
    for i in lis:
        print(i)

def explicit_list_normal(lis):
    for i in lis:
        print(i)

def explicit_list_break(lis):
    for i in lis:
        if (i == 'Florida'): break
        else: print(i)

def explicit_list_next(lis):
    for i in lis:
        if (i == 'Alabama'): continue
        else: print(i)


# The program itself
print('Program Requires:\n1. Print while loop.\n2. Print for loops using range() function, and implicit and explicit list.\n3. Use break and continue statements.\n4. Replicate display below.\nNote: In Python, for loop used for iterating over a sequence (i.e. list, tuple, dictionary, set, or string).\n')
i = 1

print('1. While loop:')
while (i <= 3):
    print(i)
    i += 1

print('\n2. For loop using range() (one arg)')
for i in range(4):
    print(i)

print('\n3. For loop using range() (two args)')
for i in range(1, 4):
    print(i)

print('\n4. For loop using range() (three args)')
for i in range(0, 4, 2):
    print(i)

print('\n5. For loop using range() (negative interval)')
for i in range(3, 0, -2):
    print(i)

print('\n6. For loop using implicit list')
implicit_list()

explicit_list = ['Michigan', 'Alabama', 'Florida']

print('\n7. For loop using explicit list')
explicit_list_normal(explicit_list)

print('\n8. For loop using break statement (Stops loop)')
explicit_list_break(explicit_list)

print('\n9. For loop using continue statement (Stops and continues with next)')
explicit_list_next(explicit_list)

print('\n10. Print list length')
print(len(explicit_list))