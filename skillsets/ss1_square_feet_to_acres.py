#!/usr/bin/env python3

print('Square Feet to Acres\n\nProgram Requirements:\n1. Research: number of square feet to acres of land\n2. Must use float data to for user input and calculation\n3. Format and round conversion to two decimal places.\n\nInput:\n')
sqft = float(input('Enter square feet: '))

# Calculation
acres = round(float(sqft / 43560.04), 2)
print('\nOutput:\n' + '{:,.2f}'.format(sqft) + ' = ' '{:,.2f}'.format(acres) + ' acres')