#!/use/bin/env python3 

# it = 25
# ict = 15

print('IT/ICT Student Percentage\n\nProgram Requirements\n1. Find number of IT/ICT students in class.\n2. Calculate IT/ICT Student Percentage\n3. Must use float data type (to facilitate right-alignment)\n4. Format, right-align numbers, and round to two decimals places.\n')

print('Input:')
cnt_it = int(input('Enter number of IT students: '))
cnt_ict = int(input('Enter number of ICT students: '))
total = float(cnt_it + cnt_ict)

print('\nOutput:')
print('{0:17} {1:>5.2f}'.format('Total Students:', total))
print('{0:17} {1:>5.2%}'.format('IT Students:', round(float(cnt_it / total), 4)))
print('{0:17} {1:>5.2%}'.format('ICT Students:', round(float(cnt_ict / total), 4)))