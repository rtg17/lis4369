# LIS4369 - A5

## A5 Requirements:

*[Getting Started with R](/A5):*

- Get started using R and R Studio

- Create a file with proper syntax and output for R

### R commands

- `setwd(dir: string)`: Change the working directory
- `install.packages(package: string)`: Allows you to install packages to use with R
- `installed.packages()`: Allows you to see the packages installed
- `library(package: string)`: Load a package into your R script
- `update.packages()`: Get package updates
- `remove.packages(package: string)`: Remove an installed package
- `?[funcName]` or `help(function: string)`: Allows you to see what you can do with a certain function
- `args(function: string)`: Returns the arguments a function takes
- `??[term]` or `help.search(term: string)`: Allows you to use the search function for specific terms
- `data()`: List pre-loaded datasets. Typing out a dataset name will allow you to print the data it contains.
- `<-`: Assignment operator
- `-`: Also an assignment operator, but not in every case
- `[var] <- read.csv(location: string, header?=TRUE|FALSE, stringsAsFactor?=TRUE|FALSE, sep?=[string])`: Allows you to pull data from a .csv file into a variable, thus initializing a dataset. 
    - Passing `header=FALSE` will only pull the data and not the column headers, while TRUE does the opposite.
    - Passing `stringAsFactor=FALSE` prevents text data from reading in as factors.
    - Passing `sep=[string]` allows you to define a delimiter for data.
    - Capable of importintg data from URLs, including link-shortened ones.
- `[var] <- read.table(file=[string]/"clipboard"|pipe("pbpaste"))`: Allows you to pass a filename, or use your OS clipboard, to read data into a variable.
    - Uses the same parameters and rules as `read.csv`.
- `write.table(dataset, filename: string, sep='\t')`: Exports the dataset into a text file with a delimiter
- `edit(dataset)`: Graphically edit the values of a dataset
- `var - array(some list of variables)`: Creates an array
- `barChart(var: dataset)` or `chartSeries(var: dataset)`: Create charts with your datasets
- `save.image()`: Saves the data as an image
- `save(var, file-'[string]')`: Save a specific piece of data, usually as a `.rda` file.
- `rx(var)`: Removes an unused variable
- `head()`: See the start of a set of data
- `tail()`: See the end of a set of data
- `str()`: Print the data as a string 
- `colnames()`: Get the column names from a set of data passd
- `rownames()`: Get the row names from a set of data passed
- `summary()`: Returns basic calculations from a set of data passed
- `describe()`: Similar to `summary()` but with additional information
- `cor()`: Returns correlation data of a data set
- `na.rm-TRUE/FALSE`: option passed into most functions to remove data which is marked with NA
- `[dataset]$[column/rowname/range]`: Get parts of data from a dataset, which can be stored into a variable
- `? <- c([some array here])`: Concatenates, or converts/coerces, depending on the situation it's used it
- `sqldf(query: string)`: Allows you to query a dataframe as if it were a SQL database

#### Assignment Screenshots:

![Running in R Studio](/img/a5.png)

![Distribution chart](/img/a5_age_dist.png)

![Age plot 1](/img/a5_age_plot1.png)

![Age plot 2](/img/a5_age_plot2.png)

![Age plot 3](/img/a5_age_plot3.png)

![Age and Survival](/img/a5_age_survival.png)

![AAPL Graph Chart](/img/a5_aapl_chart.png)

![AAPL Exported Table](/img/a5_writetable.png)