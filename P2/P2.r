# Some vars and setup
library(dplyr)
library(psych)
library(ggplot2)
intro <- 'R Language Reference Notes:\n\tUse head and tail to look at first few and last few records.\n\tUse str and names to look at structure and column names of a data frame.\n\tUse $ notation to look at a particular column name.\n\tUse [] square brackets (row,column) notation to look at a particular value.\n\n\tAlso, conditional selection in R:\n\tSelect data in I row and J column (one field) for DataFrameX: DataFrameX[I,J]\n\tAlternatively:\n\tData in I row: DataFrameX[I,] # display row/record I with column names\n\tData in J column: DataFrameX$J_Column_Name, or DataFrameX[,J]\n\tNOTE: R uses 1 for first record/row. Python uses 0!\n\n*** Assignment Requirements ***\n\n1. Use Assignment 5 screenshots and R Manual to backward-engineer the following requirements:\n2. Resources:\n\ta. R Manual: https://cran.r-project.org/doc/manuals/r-release/R-lang.pdf\n\tb. R for Data Science: https://r4ds.had.co.nz/\n3. Use Motor Trend Car Road Tests data:\n\ta. Research the data! https://stat.ethz.ch/R-manual/R-devel/library/datasets/html/mtcars.html\n\tb. url = "http://vincentarelbundock.github.io/Rdatasets/csv/datasets/mtcars.csv"\nNote: Use variable "mtcars" to read file into. (See Assignment 5 for reading .csv files.)'
poundbar <- '###################################################'
mtcars <- read.csv(file='vincentarelbundock.github.io/Rdatasets/csv/datasets/mtcars.csv',header=TRUE,sep=',')

# Program requirements
cat('\014')
message(poundbar)
message(intro)
message(poundbar)

#Output 
message('1) Display all data from file')
print(mtcars)

message('2) Display first 10 records')
print(head(mtcars,n=10))

message('3) Display last 10 records')
print(tail(mtcars,n=10))

message('4) Display file structure')
print(str(mtcars))

message('5) Display column names')
print(colnames(mtcars))

message('6) Display first record/row with column names')
print(mtcars[1,])

message('7) Display mpg column data without showing header')
print(mtcars[,1])

message('8) Display cyl column (using column name)')
print(mtcars[,'cyl'])

message('9) Display row/column 3/4 using bracket notation')
print(mtcars[3,4])

message('10) Display all cars with more than four cylinders')
print(filter(mtcars, mtcars$cyl > 4))

message('11) Display cars with more than four cylinders and at least five gears')
print(filter(mtcars, mtcars$cyl > 4 & mtcars$gear >= 5))

message('12) Display cars with more than four cylinders and exactly four gears')
print(filter(mtcars, mtcars$cyl > 4 & mtcars$gear == 4))

message('13) Display cars with more than four cylinders or exactly four gears')
print(filter(mtcars, mtcars$cyl > 4 | mtcars$gear == 4))

message('14) Display cars with more than four cylinders without four gears')
print(filter(mtcars, mtcars$cyl > 4 & mtcars$gear != 4))

message('15) Display total number of rows (only the number)')
print(dim(mtcars)[1])

message('16) Display total number of columns (only the number)')
print(dim(mtcars)[2])

message('17) Display total number of rows and columns (only the number)')
print(dim(mtcars))

message('18) Display data frame structure (same info as in Python)')
print(str(mtcars))

message('19) Get mean, median, minimum, maximum, quantiles, variance, and standard deviation of horsepower')
cat('a. Mean: ', mean(mtcars$hp, na.rm=TRUE))
cat('a. Median: ', median(mtcars$hp, na.rm=TRUE))
cat('a. Mean: ', min(mtcars$hp, na.rm=TRUE))
cat('a. Mean: ', max(mtcars$hp, na.rm=TRUE))
cat('e. Quantile:')
quantile(mtcars$hp, na.rm=TRUE)
cat('f. Variance: ', var(mtcars$hp, na.rm=TRUE))
cat('g. Standard Deviation: ', sd(mtcars$hp, na.rm=TRUE))

message('20) Get summary() of dataset')
print(summary(mtcars))

# Plot some data
#qplot(disp, mpg, data=mtcars, colour=cyl) + labs(title = 'Displacement vs MPG', x = 'Displacement', y = 'MPG')
#plot(mtcars$wt, mtcars$mpg, xlab='Weight in Thousands', ylab='MPG', main='Weight vs MPG')