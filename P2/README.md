# LIS4369 - P2

## P2 Requirements:

*[R data science](/P2):*

- Reverse engineer a provided text file's output

- Research and implement data science methods in R

- Create and style/label graphs with data, using R's inherent `plot()` function as well as ggplot2's `qplot()` and `labs()`

#### Assignment Screenshots:

![RStudio output](/img/p2_output.png)

![Displacement vs MPG graph](/img/p2_disp_graph.png)

![Weight vs MPG graph](/img/p2_weight_graph.png)